Evolution Mail (Linux Users)



The Evolution Mail client supports Office 365 through the evolution-ews plugin, starting in evolution-ews v3.27.91 and above. The plugin provides Exchange Web Services and modern authentication support, required to access Office 365 mail with Okta MFA. Ubuntu 18.04 and Fedora 28 are known to be working, although any Linux distribution with evolution-ews v3.27.91 or above in it's package repository should theoretically work.